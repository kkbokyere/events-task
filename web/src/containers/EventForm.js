import { connect } from 'react-redux'
import EventForm from "../components/EventForm";
import {fetchCategories} from "../state/ducks/categories";
import {fetchCoordinators} from "../state/ducks/coordinators";
import {addEvent} from "../state/ducks/events";
import {createCategoryOptions, createCoordinatorOptions} from "../state/ducks/selectors";
const mapStateToProps = ({ coordinators, categories }, ownProps) => {
    return {
        coordinators: createCoordinatorOptions(coordinators.data),
        categories: createCategoryOptions(categories.data),
        ...ownProps
    }
};

const mapDispatchToProps = (dispatch, { handleFormComplete }) => {
    return {
        submitForm: (eventData) => {
            dispatch(addEvent(eventData));
            handleFormComplete();
        },
        handleFetchData: () => {
            dispatch(fetchCategories());
            dispatch(fetchCoordinators());
        }
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EventForm)
