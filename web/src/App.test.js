import React from "react";
import { render, fireEvent, cleanup, waitForElement } from './tests/helper'
import App from "./App";

describe('App Tests', () => {
  afterEach(cleanup);

  const setup = (props) => {
    return render(<App/>, props);
  };

  describe('Setup', () => {
    it('should render app', () => {
      const { asFragment } = setup();
      expect(asFragment()).toMatchSnapshot();
    });

    it('should render form elements', () => {
      const { getByTestId } = setup();
      expect(getByTestId('event-form')).toBeInTheDocument();
    });

    it('should fetch categories data in select box', async () => {
      const { queryByTestId } = setup();
      expect(queryByTestId('category_id-select')).not.toBeInTheDocument();
      const categoriesSelect = await waitForElement(() => queryByTestId('category_id-select'));
      expect(categoriesSelect.children.length).toBe(2);
      expect(categoriesSelect).toBeInTheDocument();
    });

    it('should fetch coordinators data in select box', async () => {
      const { queryByTestId } = setup();
      expect(queryByTestId('coordinator-select')).not.toBeInTheDocument();
      const coordinatorSelect = await waitForElement(() => queryByTestId('coordinator-select'));
      expect(coordinatorSelect.children.length).toBe(3);
      expect(coordinatorSelect).toBeInTheDocument();
    });
  });

  describe('Interactions', () => {
    it('should update char limit when description is filled', () => {
      const { queryByTestId } = setup();
      expect(queryByTestId('char-counter')).toHaveTextContent('140 / 140');
      fireEvent.change(queryByTestId('description-textarea'), { target: { value: "some description"}});
      expect(queryByTestId('char-counter')).toHaveTextContent('124 / 140');
    });
    it('should toggle fee input', () => {
      const { queryByTestId } = setup();
      expect(queryByTestId('event_fee-input')).toBeDisabled();
      fireEvent.click(queryByTestId('paid_event-radio'));
      expect(queryByTestId('event_fee-input')).not.toBeDisabled();
    });
  });

  describe('Form submission', () => {
    it('should complete form submission', async () => {
      const { queryByTestId } = setup();
      const coordinatorsSelect = await waitForElement(() => queryByTestId('coordinator-select'));
      const categoriesSelect = await waitForElement(() => queryByTestId('category_id-select'));
      fireEvent.change(queryByTestId('title-input'), { target: { value: "some title"}});
      fireEvent.change(queryByTestId('description-textarea'), { target: { value: "some description"}});
      fireEvent.change(categoriesSelect, { target: { value: 0}});
      fireEvent.click(queryByTestId('paid_event-radio'));
      fireEvent.change(queryByTestId('reward-input'), { target: { value: "10"}});
      fireEvent.change(coordinatorsSelect, { target: { value: 0}});
      fireEvent.change(queryByTestId('email-input'), { target: { value: "kkbokyere@gmail.com"}});
      fireEvent.change(queryByTestId('date-input'), { target: { value: "2020-10-12"}});
      fireEvent.change(queryByTestId('time-input'), { target: { value: "00:10"}});
      fireEvent.change(queryByTestId('duration-input'), { target: { value: "1"}});
      fireEvent.submit(queryByTestId('event-form'));
      // success alert
      const alertBox = queryByTestId('alert-box');
      expect(alertBox).toBeInTheDocument();
      expect(alertBox).toHaveTextContent('Success')
    });
  })
});
