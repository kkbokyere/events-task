import React from 'react';
import FormGroup from "../components/FormGroup";

const withFormGroup = (WrappedComponent) => {
    return (props) => {
        return (
            <FormGroup labelName={props.name} labelTitle={props.title} {...props}>
                <WrappedComponent {...props} />
            </FormGroup>
            );
    };
};

export default withFormGroup;
