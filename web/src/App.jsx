import React, {useState} from "react";
import "./styles/App.scss";
import Alert from "./components/Alert";
import Layout from "./components/Layout";
import Header from "./components/Header";
import EventForm from "./containers/EventForm";

function App() {
    const [confirmation, showConfirmation] = useState(false);
    const handleFormSuccess = () => {
        showConfirmation(true)
    };
  return (
    <div className="App">
      <Header />
      <Layout>
          {confirmation && <Alert type='success' title='Success' message='Event has been created'/>}
          {!confirmation && <EventForm handleFormComplete={handleFormSuccess} />}
      </Layout>
    </div>
  );
}

export default App;
