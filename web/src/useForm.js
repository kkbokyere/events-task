import { useState, useEffect } from 'react';

const useForm = (submitCallback, validate) => {

    const [formData, setFormData] = useState({});
    const [errors, setErrors] = useState({});
    const [isSubmitting, setIsSubmitting] = useState(false);

    useEffect(() => {
        if (Object.keys(errors).length === 0 && isSubmitting) {
            submitCallback();
        }
    }, [isSubmitting, errors, submitCallback]);

    useEffect(() => {
        setErrors(validate(formData));
    }, [formData, validate]);

    const handleSubmit = (event) => {
        if (event) event.preventDefault();
        setErrors(validate(formData));
        setIsSubmitting(true)
    };

    const handleOnChange = (event) => {
        event.persist();
        setFormData(values => ({ ...values, [event.target.name]: event.target.value }));
    };

    return {
        handleOnChange,
        handleSubmit,
        formData,
        errors
    }
};

export default useForm;
