import {CATEGORIES_API_ENDPOINT,API_URL} from "../../state/api/categories";
import {COORDINATORS_API_ENDPOINT} from "../../state/api/coordinators";
import {mockCategoriesResponse, mockCoordinatorsResponse} from "./data";

export default {
  get: jest.fn((url) => {
    switch (url) {
      case `${API_URL}/${CATEGORIES_API_ENDPOINT}`:
        return Promise.resolve({ data: mockCategoriesResponse });
      case `${API_URL}/${COORDINATORS_API_ENDPOINT}`:
        return Promise.resolve({ data: mockCoordinatorsResponse });
      default:
        return Promise.resolve({ data: [{}] })

    }
  }),
};
