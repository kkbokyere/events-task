export const mockCategoriesResponse = [{
id: 0,
    name: 'Cycling'
}];

export const mockCoordinatorsResponse = [{
    id: 0,
    name: 'Daniel',
    lastname: 'Mitchell',
    email: "daniel.mitchell@hussa.rs"
},{
    id: 1,
    name: 'Kingsley',
    lastname: 'Okyere',
    email: "kingsley.okyere@hussa.rs"
}];
