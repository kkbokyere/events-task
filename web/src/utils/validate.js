export default function handleValidation (values) {
    let errors = {};
    if (!values.email) {
        errors.email = 'Email address is required';
    } else if (!/\S+@\S+\.\S+/.test(values.email)) {
        errors.email = 'Email address is invalid';
    }
    if (!values.title) {
        errors.title = 'Title is required';
    }
    if (!values.description) {
        errors.description = 'Description is required';
    }

    if (!values.coordinator) {
        errors.coordinator = 'Coordinator is required';
    }
    if (!values.time || !values.date) {
        errors.dateTime = 'Date & Time is required';
    } else if (!/\d{4}-\d{2}-\d{2}/.test(values.date)) {
        errors.dateTime = 'Date is invalid';
    }


    return errors
};
