import React from "react";
import cx from "classnames"
import PropTypes from "prop-types";
import styles from "./Textfield.module.scss";

const TextField = ({ name, size, title, value, handleChange, placeholder, ...props}) => {
    const inputClasses = cx(styles.input, {
       [styles.inputSmall]: size === 'sm',
       [styles.inputMedium]: size === 'md',
       [styles.inputFull]: size === 'fullwidth',
    });
  return (
      <input
          data-testid={`${name}-input`}
          className={inputClasses}
          id={name}
          value={value}
          onChange={handleChange}
          placeholder={placeholder}
          name={name}
          {...props}
      />
  );
};

TextField.defaultProps = {
    size: "fullwidth"
};

TextField.propTypes = {
    name: PropTypes.string,
    title: PropTypes.string,
    type: PropTypes.string,
    size: PropTypes.oneOf(["sm", "md", "fullwidth"]),
    defaultProps: PropTypes.string,
    placeholder: PropTypes.string,
    handleChange: PropTypes.func,
};

export default TextField;
