import React from "react";

import styles from "./FormGroup.module.scss";
import PropTypes from "prop-types";

const FormGroup = ({ children, title, ...props }) => {
  return (
    <div className={styles.formGroup}>
      <label htmlFor={props.labelName} className={styles.formGroupLabel}>
        {`${props.labelTitle} ${props.required ? '*': ''}`}
      </label>
      {children}
        <span className={styles.formGroupHelper}>{props.helperText}</span>
    </div>
  );
};

FormGroup.propTypes = {
  labelName: PropTypes.string,
  labelTitle: PropTypes.string,
};
export default FormGroup;
