import React from "react";
import PropTypes from "prop-types";

import styles from "./Fieldset.module.scss";

const Fieldset = ({ title, children, ...props }) => {
  return (
    <fieldset className={styles.fieldset} {...props}>
      <header className={styles.fieldsetHeader}>{title}</header>
      <div className={styles.fieldsetBody}>{children}</div>
    </fieldset>
  );
};

Fieldset.propTypes = {
  children: PropTypes.any,
  handleUpdateForm: PropTypes.func,
};

export default Fieldset;
