import React from "react";
import {fireEvent, render} from "../../tests/helper";
import DateSelector from "./DateSelector";
describe("DateSelector Tests", () => {
  const setup = (props) => {
    return render(<DateSelector {...props} />);
  };
  it("should render DateSelector", async () => {
    const { asFragment } = setup();

    expect(asFragment()).toMatchSnapshot();
  });

  it("should call handleUpdate with date", () => {
    const handleOnUpdateMock = jest.fn();
    const { queryByTestId } = setup({ handleOnUpdateDate: handleOnUpdateMock });
    fireEvent.change(queryByTestId('date-input'), { target: { value: "2020-10-12"}});
    fireEvent.change(queryByTestId('time-input'), { target: { value: "00:10"}});
    expect(handleOnUpdateMock).toHaveBeenCalledTimes(2)
  });

});
