import React from "react";
import styles from "./DateSelector.module.scss";
import TextField from "../Textfield";
import PropTypes from "prop-types";
import RadioGroup from "../RadioGroup";

const meridiemOptions = [
    {
        label: "am",
        text: "AM",
        name: "meridiem",
        value: "am"
    },
    {
        label: "pm",
        text: "PM",
        name: "meridiem",
        value: "pm"
    }
];

const DateSelector = ({ handleOnUpdateDate }) => {
    return(
        <div className={styles.dateSelector}>
            <TextField title="date"
                       required
                       placeholder="dd/mm/yyyy"
                       pattern="\d{4}-\d{2}-\d{2}"
                       type="date"
                       size="fullwidth"
                       name="date"
                       maxLength="10"
                       handleChange={handleOnUpdateDate}/>
                       <div>at</div>
            <TextField name="time" title="time" maxLength="4" type="time" min="00:00" max="23:59" required placeholder="--:--" handleChange={handleOnUpdateDate}/>
            <RadioGroup options={meridiemOptions} selectedOption="am" title="meridiem" name="meridiem" handleChange={handleOnUpdateDate}/>
        </div>
    )
};

DateSelector.defaultProps = {
    handleOnUpdateDate: () => {}
};

DateSelector.propTypes = {
    handleOnUpdateDate: PropTypes.func,
};

export default DateSelector;
