import React, {useEffect} from "react";
import PropTypes from "prop-types";
import Fieldset from "../Fieldset";
import Button from "../Button";
import Input from "../Input";
import Select from "../Select";
import Textarea from "../Textarea";
import RadioGroup from "../RadioGroup";
import DateSelector from "../DateSelector";
import CharacterCounter from "../CharacterCounter/CharacterCounter";
import TextField from "../Textfield";
import withFormGroup from "../../hocs/withFormGroup";
import useForm from "../../useForm";
import handleValidation from "../../utils/validate";
import Alert from "../Alert";

//Wrapped components with HOC
const InputComponent = withFormGroup(Input);
const DateSelectorComponent = withFormGroup(DateSelector);
const RadioGroupComponent = withFormGroup(RadioGroup);
const SelectComponent = withFormGroup(Select);
const TextareaComponent = withFormGroup(Textarea);

const paidEventOptions = [{
    label: "free_event",
    text: "Free Event",
    name: "paid_event",
    value: false
},
    {
        label: "paid_event",
        text: "Paid Event",
        name: "paid_event",
        value: true
    }];

const EventForm = ({ submitForm, handleFetchData, coordinators, categories }) => {
    const { formData, handleOnChange, handleSubmit, errors } = useForm(handleOnSubmit, handleValidation);
    const isPaidEvent = formData.paid_event === 'true';
    function handleOnSubmit() {
        submitForm(formData);
    }
  useEffect(() => {
      handleFetchData()
  }, [handleFetchData]);
  return (
    <form onSubmit={handleSubmit} data-testid='event-form'>
      <Fieldset title="About">
          <InputComponent title="Title" placeholder="Make it short and clear" name="title" required handleChange={handleOnChange}/>
          {errors.title && <Alert type="error" size="sm" message={errors.title}/>}

          <TextareaComponent title="Description" type="textarea" maxLength={140} name="description" placeholder="Write about your event, be creative" required handleChange={handleOnChange}>
              Characters remaining: <CharacterCounter content={formData.description} charLimit={140}/>
          </TextareaComponent>
          {errors.description && <Alert type="error" size="sm" message={errors.description}/>}

          <SelectComponent
              options={categories}
              title="category"
              name="category_id"
              handleChange={handleOnChange}
              placeholder={"Please select"}
          />
          <RadioGroupComponent options={paidEventOptions} selectedOption="free_event" title="payment" name="paid_event" handleChange={handleOnChange}>
              <TextField placeholder="Fee" handleChange={handleOnChange} name="event_fee" size="sm" type="number" disabled={!isPaidEvent} required={isPaidEvent}/>
          </RadioGroupComponent>
          <InputComponent sideText="reward points for attendance" title="reward" size="sm" name="reward" type="number" handleChange={handleOnChange} placeholder="Number"/>
      </Fieldset>
      <Fieldset title="Coordinator">
          <SelectComponent options={coordinators} title="responsible" name="coordinator" handleChange={handleOnChange} required/>
          <InputComponent title="Email" placeholder="Email" name="email" type="email" required handleChange={handleOnChange}/>
          {(errors.coordinator || errors.email) && <Alert type="error" size="sm" message={errors.coordinator || errors.email}/>}
      </Fieldset>
      <Fieldset title="When">
          <DateSelectorComponent labelTitle="starts on" labelName="starts on" required handleOnUpdateDate={handleOnChange}/>
          <InputComponent type="number" sideText="hour" size="sm" title="duration" placeholder="number" name="duration" handleChange={handleOnChange}/>
          {errors.dateTime && <Alert size="sm" type="error" message={errors.dateTime}/>}
      </Fieldset>
      <Button onClick={handleOnSubmit} type="button">
        Publish Event
      </Button>
    </form>
  );
};

EventForm.defaultProps = {
    handleFetchData: () => {},
};

EventForm.propTypes = {
  children: PropTypes.any,
    handleFetchData: PropTypes.func,
};

export default EventForm;
