import React from "react";

import styles from "./Header.module.scss";

const Header = () => {
  return (
    <header className={styles.header}>
      <div className={styles.headerContainer}>
        <h1>New Event</h1>
      </div>
    </header>
  );
};

export default Header;
