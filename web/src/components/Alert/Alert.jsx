import React from "react";
import PropTypes from 'prop-types';
import cx from 'classnames';
import styles from './Alert.module.scss'

const Alert = ({ title, message, type, size }) => {
    const alertClasses = cx(styles.alert, {
        [styles[type]]: true,
        [styles[size]]: true
    });
    return (<div className={alertClasses} data-testid="alert-box">
        {title && <h2 className={styles.title}>{title}</h2>}
        <p className={styles.message}>{message}</p>
    </div>)
};

Alert.propTypes = {
    title: PropTypes.string,
    message: PropTypes.string,
    type: PropTypes.oneOf(['success', 'error']),
    size: PropTypes.oneOf(['sm', 'md', 'lg'])
};

export default Alert;
