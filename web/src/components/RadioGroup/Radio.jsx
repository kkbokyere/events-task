import React from "react";
import PropTypes from "prop-types";
import styles from "./RadioGroup.module.scss"
const Radio = ({ value, text, name, label, handleChange, defaultChecked}) => {
    return (
        <label key={value} htmlFor={label} className={styles.radioInput} data-testid={`${label}-label`}>
            <input
                data-testid={`${label}-radio`}
                id={label}
                name={name}
                onChange={handleChange}
                value={value}
                defaultChecked={defaultChecked}
                type="radio"
            />{" "}
            {text}
        </label>
    );
};

Radio.defaultProps = {
    defaultChecked: false,
    handleChange: () => {}
};

Radio.propTypes = {
    name: PropTypes.string,
    placeholder: PropTypes.string,
    defaultChecked: PropTypes.bool,
    options: PropTypes.array,
    handleChange: PropTypes.func,
};

export default Radio;
