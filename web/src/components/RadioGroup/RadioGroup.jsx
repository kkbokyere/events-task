import React from "react";
import PropTypes from "prop-types";
import Radio from "./Radio";

const RadioGroup = ({ options, selectedOption, children, ...props}) => {
  return (
      <div>
        {options.map((option) => {
          return (
              <Radio defaultChecked={option.label === selectedOption} key={option.label} {...option} {...props}/>
          );
        })}
        {children}
      </div>
  );
};

RadioGroup.defaultProps = {
  options: [],
  handleChange: () => {}
};

RadioGroup.propTypes = {
  name: PropTypes.string,
  defaultProps: PropTypes.string,
  placeholder: PropTypes.string,
  options: PropTypes.array,
  handleChange: PropTypes.func,
};

export default RadioGroup;
