import React from "react";
import { render } from "../../tests/helper";
import RadioGroup from "./RadioGroup";
describe("RadioGroup Tests", () => {
  const setup = (props) => {
    return render(<RadioGroup {...props} />);
  };
  it("should render RadioGroup", async () => {
    const { asFragment } = setup();

    expect(asFragment()).toMatchSnapshot();
  });


  it("should render RadioGroup with options", async () => {
    const { queryByTestId } = setup({ options: [{
      name: 'king',
        label: 'king_label',
        text: 'king text'
      },{
      name: 'king2',
        label: 'king2_label',
        text: 'king2 text'
      }],
      selectedOption: 'king_label'
    });
    const radioBtnLabel = queryByTestId('king_label-label');
    const radioBtn1 = queryByTestId('king_label-radio');

    const radioBtnLabel2 = queryByTestId('king2_label-label');
    const radioBtn2 = queryByTestId('king2_label-radio');
    expect(radioBtnLabel).toBeInTheDocument();
    expect(radioBtnLabel2).toBeInTheDocument();
    expect(radioBtnLabel).toHaveTextContent('king text');
    expect(radioBtnLabel2).toHaveTextContent('king2 text');
    expect(radioBtn1).toBeChecked();
    expect(radioBtn2).not.toBeChecked();
  });
});
