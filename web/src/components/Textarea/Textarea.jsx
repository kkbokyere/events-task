import React from "react";
import PropTypes from "prop-types";
import styles from "./Textarea.module.scss";

const Textarea = ({name, title, handleChange, children, ...props}) => {
  return (
      <div className={styles.textareaContainer}>
      <textarea
          data-testid={`${name}-textarea`}
          className={styles.textarea}
          id={name}
          name={name}
          onChange={handleChange}
          {...props}
      />
          {children}
      </div>
  );
};

Textarea.defaultProps = {
    rows: 4
};

Textarea.propTypes = {
    name: PropTypes.string,
    title: PropTypes.string,
    type: PropTypes.string,
    helperText: PropTypes.string,
    handleChange: PropTypes.func,
};

export default Textarea;
