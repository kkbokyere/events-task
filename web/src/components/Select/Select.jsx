import React from "react";
import PropTypes from "prop-types";
import styles from "./Select.module.scss";

const Select = ({ name, title, value, placeholder, options, handleChange, ...props}) => {
    if(options.length < 1) {
        return null
    }
  return (
      <select
          data-testid={`${name}-select`}
          className={styles.select}
          name={name}
          value={value}
          defaultValue={""}
          onChange={handleChange}
      >
          <option defaultChecked disabled value="">
              {placeholder}
          </option>
          {options.map((option, i) => {
              return (
                  <option key={`${option.name}-${i}`} value={option.value}>
                      {option.text}
                  </option>
              );
          })}
      </select>
  );
};

Select.defaultProps = {
    placeholder: "Please select"
};

Select.propTypes = {
  name: PropTypes.string,
  title: PropTypes.string,
  placeholder: PropTypes.string,
  options: PropTypes.array,
  handleChange: PropTypes.func,
};

export default Select;
