import React from "react";
import PropTypes from "prop-types";
import TextField from "../Textfield";
import styles from "./Input.module.scss"
const Input = ({ name, title, sideText, ...props}) => {
  return (
      <div>
          <TextField name={name} title={title} {...props}/>
          {sideText && <span className={styles.sideText}>{sideText}</span>}
      </div>
  );
};

Input.propTypes = {
    name: PropTypes.string,
    title: PropTypes.string,
    handleChange: PropTypes.func,
};

export default Input;
