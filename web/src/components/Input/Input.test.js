import React from "react";
import { render } from "../../tests/helper";
import Input from "./Input";
describe("Input Tests", () => {
  const setup = (props) => {
    return render(<Input {...props} />);
  };
  it("should render Input", async () => {
    const { asFragment } = setup();

    expect(asFragment()).toMatchSnapshot();
  });

  it("should render sidetext", async () => {
    const { getByText } = setup({ sideText: 'hello side text'});
    expect(getByText('hello side text')).toBeInTheDocument()
  });
});
