import React from "react";
import PropTypes from "prop-types";

const CharacterCounter = ({ charLimit, content }) => {
    return <span data-testid="char-counter">{charLimit - content.length} / {charLimit}</span>
};

CharacterCounter.defaultProps = {
    charLimit: 0,
    content: ''
};

CharacterCounter.propTypes = {
    content: PropTypes.string,
    charLimit: PropTypes.number,
};

export default CharacterCounter;
