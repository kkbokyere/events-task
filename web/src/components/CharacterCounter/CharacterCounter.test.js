import React from "react";
import { render } from "../../tests/helper";
import CharacterCounter from "./CharacterCounter";
describe("CharacterCounter Tests", () => {
  const setup = (props) => {
    return render(<CharacterCounter {...props} />);
  };
  it("should render CharacterCounter", async () => {
    const { asFragment } = setup();

    expect(asFragment()).toMatchSnapshot();
  });

  it("should render CharacterCounter without count", async () => {
    const { getByTestId } = setup({ charLimit: 10, content: '' });
    expect(getByTestId('char-counter')).toHaveTextContent('10 / 10')
  });

  it("should render CharacterCounter with updated count", async () => {
    const { getByTestId } = setup({ charLimit: 10, content: 'kin' });
    expect(getByTestId('char-counter')).toHaveTextContent('7 / 10')
  });
});
