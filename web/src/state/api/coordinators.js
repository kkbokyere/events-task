import axios from "axios";
export const API_URL = process.env.REACT_APP_API_URL;
export const COORDINATORS_API_ENDPOINT = process.env.REACT_APP_COORDINATORS_API_ENDPOINT;

export const getCoordinators = () => {
  return axios.get(`${API_URL}/${COORDINATORS_API_ENDPOINT}`).then((response) => {
    return response.data;
  });
};
