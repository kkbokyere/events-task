import axios from "axios";
export const API_URL = process.env.REACT_APP_API_URL;
export const CATEGORIES_API_ENDPOINT = process.env.REACT_APP_CATEGORIES_API_ENDPOINT;

export const getCategories = () => {
  return axios.get(`${API_URL}/${CATEGORIES_API_ENDPOINT}`).then((response) => {
    return response.data;
  });
};
