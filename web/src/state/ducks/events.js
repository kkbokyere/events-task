import moment from 'moment'
export const ADD_EVENT = "ADD_EVENT";

export const initialState = {
  error: null,
  data: {
    title: '',
    description: '',
    category_id: null,
    paid_event: false,
    event_fee: 0,
    reward: 0,
    date: '',
    duration: 0,
    coordinator: {
      email:'',
      id:''
    }
  },
};
export function addEvent(payload) {
  return (dispatch, getState) => {
    dispatch({
      type: ADD_EVENT,
      payload,
    });
    console.log(getState().events.data)
  }
}

export default (state = initialState, action) => {
  let { payload } = action;
  switch (action.type) {
    case ADD_EVENT:
      const {email, coordinator, date, meridiem, duration, paid_event, time, ...rest} = payload;
      const updateDateTime = moment(`${date}T${time}`).format(`YYYY-MM-DDTHH:mm`);
      const durationToSeconds = Number(duration) * 3600;
      return {
        ...state,
        data: {...rest,
          category_id: Number(rest.category_id),
          event_fee: Number(rest.event_fee),
          reward: Number(rest.reward),
          paid_event: paid_event === 'true',
          duration: durationToSeconds,
          date: updateDateTime, coordinator: { email, id: coordinator }},
      };
    default:
      return state;
  }
};
