import { getCoordinators } from "../api/coordinators";

export const GET_COORDINATORS_REQUEST = "GET_COORDINATORS_REQUEST";
export const GET_COORDINATORS_SUCCESS = "GET_COORDINATORS_SUCCESS";
export const GET_COORDINATORS_FAILURE = "GET_COORDINATORS_FAILURE";

export const initialState = {
  isLoading: false,
  error: null,
  data: [],
};

function getCoordinatorsRequest(payload) {
  return {
    type: GET_COORDINATORS_REQUEST,
    payload,
  };
}

function getCoordinatorsSuccess(payload) {
  return {
    type: GET_COORDINATORS_SUCCESS,
    payload,
  };
}

function getCoordinatorsFailure(payload) {
  return {
    type: GET_COORDINATORS_FAILURE,
    payload,
  };
}

export function fetchCoordinators(params) {
  return function (dispatch) {
    dispatch(getCoordinatorsRequest(params));
    return getCoordinators(params)
      .then((response) => {
        dispatch(getCoordinatorsSuccess(response));
      })
      .catch((error) => {
        dispatch(getCoordinatorsFailure(error));
      });
  };
}

export default (state = initialState, action) => {
  let { payload } = action;
  switch (action.type) {
    case GET_COORDINATORS_SUCCESS:
      return {
        ...state,
        data: payload,
        isLoading: false,
      };
    case GET_COORDINATORS_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: payload,
      };
    case GET_COORDINATORS_REQUEST:
      return {
        ...state,
        error: null,
        isLoading: true,
      };
    default:
      return state;
  }
};
