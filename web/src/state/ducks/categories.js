import { getCategories } from "../api/categories";

export const GET_CATEGORIES_REQUEST = "GET_CATEGORIES_REQUEST";
export const GET_CATEGORIES_SUCCESS = "GET_CATEGORIES_SUCCESS";
export const GET_CATEGORIES_FAILURE = "GET_CATEGORIES_FAILURE";

export const initialState = {
  isLoading: false,
  error: null,
  data: [],
};

function getCategoriesRequest(payload) {
  return {
    type: GET_CATEGORIES_REQUEST,
    payload,
  };
}

function getCategoriesSuccess(payload) {
  return {
    type: GET_CATEGORIES_SUCCESS,
    payload,
  };
}

function getCategoriesFailure(payload) {
  return {
    type: GET_CATEGORIES_FAILURE,
    payload,
  };
}

export function fetchCategories(params) {
  return function (dispatch) {
    dispatch(getCategoriesRequest(params));
    return getCategories(params)
      .then((response) => {
        dispatch(getCategoriesSuccess(response));
      })
      .catch((error) => {
        dispatch(getCategoriesFailure(error));
      });
  };
}

export default (state = initialState, action) => {
  let { payload } = action;
  switch (action.type) {
    case GET_CATEGORIES_SUCCESS:
      return {
        ...state,
        data: payload,
        isLoading: false,
      };
    case GET_CATEGORIES_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: payload,
      };
    case GET_CATEGORIES_REQUEST:
      return {
        ...state,
        error: null,
        isLoading: true,
      };
    default:
      return state;
  }
};
