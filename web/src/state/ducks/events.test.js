import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as actions from './events'
import reducer, { initialState, ADD_EVENT } from './events'
const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
describe('actions', () => {
    it('should create an action to add a todo', () => {
        const payload = {
            title: 'king',
            date: '2020-10-01',
            time: '00:10'
        };
        const expectedAction = [{
            type: ADD_EVENT,
            payload
        }];
        const store = mockStore({ events: { data: {}} });
        store.dispatch(actions.addEvent(payload));
        expect(store.getActions()).toEqual(expectedAction)
    })
});

describe('reducer', () => {
    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState)
    });
    it('should handle ADD_EVENT', () => {
        expect(
            reducer(initialState, {
                type: ADD_EVENT,
                payload: {
                    email: 'kkbokyere@gmail.com',
                    coordinator: '1',
                    date: '2020-01-10',
                    time: '00:10',
                    duration: "1",
                    title: 'king title',
                    description: 'king description',
                    event_fee: '1',
                    category_id: '1',
                    reward: '1',
                    paid_event: 'false'
                }
            })
        ).toEqual({
            ...initialState,
            data: {
                description: 'king description',
                title: 'king title',
                event_fee: 1,
                category_id: 1,
                reward: 1,
                paid_event: false,
                date: '2020-01-10T00:10',
                duration:3600,
                coordinator: {
                    id: '1',
                    email: 'kkbokyere@gmail.com'
                }
            }
        })
    })

});
