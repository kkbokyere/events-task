import { createSelector } from 'reselect'

const getData = (data) => data;

export const createCategoryOptions = createSelector(
    [getData],
    (categories) => {
        return categories.map((data) => ({
            label: data.name,
                name: 'categories',
                value: data.id,
                text: data.name
        }))
    });

export const createCoordinatorOptions = createSelector(
    [getData],
    (coordinators) => {
        return coordinators.map((data) => ({
            label: data.name,
            name: 'coordinators',
            value: data.id,
            text: data.name
        }))
    });
